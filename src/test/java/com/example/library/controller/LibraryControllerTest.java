package com.example.library.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.library.LibraryApplication;
import com.example.library.TestingService;
import com.example.library.constant.Constants;
import com.example.library.constant.Endpoints;
import com.example.library.domain.request.NewLibrary;
import com.example.library.domain.response.Response;
import com.example.library.entity.Library;
import com.example.library.service.BookService;
import com.example.library.service.LibraryService;
import java.io.IOException;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(classes = LibraryApplication.class)
public class LibraryControllerTest extends TestingService {

  protected MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @MockBean
  private LibraryService libraryService;
  @MockBean
  private BookService bookService;

  @BeforeEach
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .apply(SecurityMockMvcConfigurers.springSecurity())
        .build();
  }

  @Test
  public void getLibraries_whenUseAnonymousUser_shouldReturnStatusUnauthorized() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(Endpoints.LIBRARIES))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithUserDetails
  public void getLibraries_whenUseNormalUser_shouldReturnStatusOk() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(Endpoints.LIBRARIES))
        .andExpect(status().isOk());
  }

  @Test
  @WithUserDetails(value = Constants.ADMIN)
  public void getLibraries_whenUseAdminUser_shouldReturnStatusOk() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(Endpoints.LIBRARIES))
        .andExpect(status().isOk());
  }

  @Test
  public void addLibrary_whenUseAnonymousUser_shouldReturnStatusUnauthorized() throws Exception {
    String library = getStringObjectFromJsonFile("/request/library/addLibrary_Ok.json", NewLibrary.class);
    mockMvc.perform(MockMvcRequestBuilders.post(Endpoints.LIBRARIES)
          .content(library))
      .andExpect(status().isUnauthorized());
  }

  @Test
  @WithUserDetails
  public void addLibrary_whenUseNormalUser_shouldReturnStatusForbidden() throws Exception {
    String library = getStringObjectFromJsonFile("/request/library/addLibrary_Ok.json", NewLibrary.class);
    mockMvc.perform(MockMvcRequestBuilders.post(Endpoints.LIBRARIES)
            .contentType(MediaType.APPLICATION_JSON)
            .content(library))
        .andExpect(status().isForbidden());
  }

  @Test
  @WithAdminDetails
  public void addLibrary_whenUseAdminUser_shouldReturnStatusOk() throws Exception {
    String library = getStringObjectFromJsonFile("/request/library/addLibrary_Ok.json", NewLibrary.class);
    Library libraryResponse = getLibraryResponseFromRequest(library);
    Response response = new Response(HttpStatus.OK.getReasonPhrase(), libraryResponse);
    String bookTitle = getBookTitle(libraryResponse);

    when(libraryService.addLibrary(any())).thenReturn(response);

    mockMvc.perform(MockMvcRequestBuilders.post(Endpoints.LIBRARIES)
            .contentType(MediaType.APPLICATION_JSON)
            .content(library))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("data.id").value(1))
        .andExpect(jsonPath("data.name").value(libraryResponse.getName()))
        .andExpect(jsonPath("data.books", hasSize(1)))
        .andExpect(jsonPath("data.books[0].title").value(bookTitle));
  }

  private String getBookTitle(Library library) {
    return library.getBooks().stream().collect(Collectors.toList()).get(0).getTitle();
  }

  private Library getLibraryResponseFromRequest(String request) throws IOException {
    NewLibrary newLibrary = getObjectFromJsonString(request, NewLibrary.class);
    newLibrary.getBooks().forEach(book -> book.setId(1));
    Library response = new Library(newLibrary.getName(), newLibrary.getBooks());
    response.setId(1);
    return response;
  }

}
