package com.example.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class TestingService {

  protected Gson gson = new Gson();

  public <T> T getObjectFromJsonFile(String path, Class<T> valueType) throws IOException {
    try (Reader reader = new InputStreamReader(this.getClass().getResourceAsStream(path))) {

      return gson.fromJson(reader, valueType);
    }
  }

  public <T> String getStringObjectFromJsonFile(String path, Class<T> valueType)
      throws IOException {
    try (Reader reader = new InputStreamReader(this.getClass().getResourceAsStream(path))) {

      return gson.toJson(gson.fromJson(reader, valueType), valueType);
    }
  }

  protected <T> T getObjectFromJsonString(String jsonString, Class<T> valueType,
                                          String... datePattern) throws IOException {
    GsonBuilder builder = new GsonBuilder();
    for (String pattern : datePattern) {
      builder.setDateFormat(pattern);
    }
    return builder.create().fromJson(jsonString, valueType);
  }

}
