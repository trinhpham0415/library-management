package com.example.library.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Pointcuts {

  @Pointcut("this(org.springframework.data.repository.PagingAndSortingRepository)")
  public void repositoryClassMethods() {}

}
