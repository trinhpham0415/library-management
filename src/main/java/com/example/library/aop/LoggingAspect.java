package com.example.library.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
public class LoggingAspect {

  private static final Logger LOGGER = LogManager.getLogger(LoggingAspect.class);

  @Around("within(com.example.library.controller..*)")
  public Object profileControllerMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
    String className = methodSignature.getDeclaringType().getSimpleName();
    String methodName = methodSignature.getName();

    final StopWatch stopWatch = new StopWatch();

    //Measure method execution time
    stopWatch.start();
    Object result = proceedingJoinPoint.proceed();
    stopWatch.stop();
    //Log method execution time
    LOGGER.info("Execution time of controller " + className + "." + methodName + " is "
      + stopWatch.getTotalTimeMillis() + " ms");
    LOGGER.info("response: " + result);
    return result;
  }

  @Around("com.example.library.aop.Pointcuts.repositoryClassMethods()")
  public Object profileRepositoryClassMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    Signature methodSignature = proceedingJoinPoint.getSignature();
    String className = methodSignature.getDeclaringType().getSimpleName();
    String methodName = methodSignature.getName();

    //Measure method execution time
    long start = System.currentTimeMillis();
    Object result = proceedingJoinPoint.proceed();
    //Log method execution time
    LOGGER.info("Execution time of repository " + className + "." + methodName + " is "
        + (System.currentTimeMillis() - start) + " ms");
    return result;
  }

}
