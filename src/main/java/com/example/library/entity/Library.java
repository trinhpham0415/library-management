package com.example.library.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Library {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  private String name;

  @OneToMany(mappedBy = "library", cascade = {CascadeType.ALL}, orphanRemoval = true)
  @JsonIgnoreProperties("library")
  private Set<Book> books;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Set<Book> getBooks() {
    return books;
  }

  public void setBooks(Set<Book> books) {
    this.books = books;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Library(String name, Set<Book> books) {
    this.name = name;
    this.books = books;
  }

  public Library(String name) {
    this.name = name;
  }

  public Library() {}

  @Override
  @JsonIgnoreProperties("library")
  public String toString() {
    return "Library{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }

}
