package com.example.library.controller;

import com.example.library.constant.Constants;
import com.example.library.constant.Endpoints;
import com.example.library.domain.request.NewBook;
import com.example.library.domain.request.NewLibrary;
import com.example.library.domain.response.Response;
import com.example.library.service.BookService;
import com.example.library.service.LibraryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.LIBRARIES)
public class LibraryController {

  private final LibraryService libraryService;
  private final BookService bookService;

  public LibraryController(LibraryService libraryService, BookService bookService) {
    this.libraryService = libraryService;
    this.bookService = bookService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Response addLibrary(@RequestBody NewLibrary newLibrary) {
    return libraryService.addLibrary(newLibrary);
  }

  @PostMapping(Endpoints.PATH_LIBRARY_ID + Endpoints.BOOKS)
  @ResponseStatus(HttpStatus.CREATED)
  public Response addNewBookToLibrary(@PathVariable(Constants.LIBRARY_ID) Long libraryId,
                                      @RequestBody NewBook newBook) {
    return bookService.addNewBookToLibrary(libraryId, newBook);
  }

  @PatchMapping(Endpoints.PATH_LIBRARY_ID + Endpoints.BOOKS + Endpoints.PATH_BOOK_ID)
  public Response linkBookToLibrary(@PathVariable(Constants.LIBRARY_ID) Long libraryId,
                                    @PathVariable(Constants.BOOK_ID) Long bookId) {
    return bookService.linkBookToLibrary(libraryId, bookId);
  }

  @GetMapping
  public Response getLibraries() {
    return libraryService.getLibraries();
  }

  @DeleteMapping(Endpoints.PATH_LIBRARY_ID)
  public Response deleteLibrary(@PathVariable(Constants.LIBRARY_ID) Long libraryId) {
    return libraryService.deleteLibrary(libraryId);
  }

}
