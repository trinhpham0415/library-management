package com.example.library.controller;

import com.example.library.constant.Endpoints;
import com.example.library.domain.request.AuthenticationRequest;
import com.example.library.domain.response.Response;
import com.example.library.service.JwtTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.AUTHENTICATE)
public class AuthenticationController {

  private final AuthenticationManager authenticationManager;
  private final UserDetailsService userDetailsService;
  private final JwtTokenService jwtTokenService;

  public AuthenticationController(
      AuthenticationManager authenticationManager,
      UserDetailsService userDetailsService,
      JwtTokenService jwtTokenService) {
    this.authenticationManager = authenticationManager;
    this.userDetailsService = userDetailsService;
    this.jwtTokenService = jwtTokenService;
  }

  @PostMapping
  public Response authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            authenticationRequest.getUsername(), authenticationRequest.getPassword()));
    UserDetails user = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
    return new Response(HttpStatus.OK.getReasonPhrase(), jwtTokenService.generateToken(user));
  }
}
