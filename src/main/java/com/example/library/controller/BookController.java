package com.example.library.controller;

import com.example.library.constant.Constants;
import com.example.library.constant.Endpoints;
import com.example.library.domain.response.Response;
import com.example.library.service.BookService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.BOOKS)
public class BookController {

  private final BookService bookService;

  public BookController(BookService bookService) {
    this.bookService = bookService;
  }

  @GetMapping
  public Response getBooks() {
    return bookService.getBooks();
  }

  @PatchMapping(Endpoints.PATH_BOOK_ID)
  public Response removeBookFromLibrary(@PathVariable(Constants.BOOK_ID) Long bookId) {
    return bookService.removeBookFromLibrary(bookId);
  }

}
