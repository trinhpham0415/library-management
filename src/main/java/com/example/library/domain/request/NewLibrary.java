package com.example.library.domain.request;

import com.example.library.entity.Book;
import java.util.Set;

public class NewLibrary {

  private String name;
  private Set<Book> books;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Book> getBooks() {
    return books;
  }

  public void setBooks(Set<Book> books) {
    this.books = books;
  }

  @Override
  public String toString() {
    return "NewLibrary{" +
        "name='" + name + '\'' +
        ", books=" + books +
        '}';
  }

}
