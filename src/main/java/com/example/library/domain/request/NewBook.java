package com.example.library.domain.request;

import com.example.library.entity.Library;

public class NewBook {

  private String title;
  private Library library;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Library getLibrary() {
    return library;
  }

  public void setLibrary(Library library) {
    this.library = library;
  }

  @Override
  public String toString() {
    return "NewBook{" +
        "title='" + title + '\'' +
        ", library=" + library +
        '}';
  }

}
