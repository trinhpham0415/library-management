package com.example.library.repository;

import com.example.library.entity.Library;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryRepository extends PagingAndSortingRepository<Library, Long> {
}
