package com.example.library.configuration;

import com.example.library.constant.Endpoints;
import com.example.library.constant.Role;
import com.example.library.security.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)
public class SecurityConfiguration {

  private final JwtRequestFilter jwtRequestFilter;
  private final HandlerExceptionResolver resolver;

  public SecurityConfiguration(JwtRequestFilter jwtRequestFilter,
         @Qualifier("handlerExceptionResolver") HandlerExceptionResolver resolver) {
    this.jwtRequestFilter = jwtRequestFilter;
    this.resolver = resolver;
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.headers().frameOptions().disable();

    httpSecurity.csrf()
      .disable()
      .authorizeRequests()
      .antMatchers(Endpoints.AUTHENTICATE)
      .permitAll()
      .antMatchers(Endpoints.PATH_H2_CONSOLE)
      .permitAll()
      .antMatchers(HttpMethod.POST)
      .hasRole(Role.ADMIN.getValue())
      .antMatchers(HttpMethod.PUT)
      .hasRole(Role.ADMIN.getValue())
      .antMatchers(HttpMethod.PATCH)
      .hasRole(Role.ADMIN.getValue())
      .antMatchers(HttpMethod.DELETE)
      .hasRole(Role.ADMIN.getValue())
      .anyRequest()
      .authenticated()
      .and()
      .httpBasic()
      .and()
      .sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    httpSecurity.exceptionHandling()
      .authenticationEntryPoint(
        (request, response, ex) -> {
          resolver.resolveException(request, response, null, ex);
        }
      );

    httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

    return httpSecurity.build();
  }

}
