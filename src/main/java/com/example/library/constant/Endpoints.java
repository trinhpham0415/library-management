package com.example.library.constant;

public class Endpoints {

  public static final String AUTHENTICATE = "/authenticate";
  public static final String LIBRARIES = "/libraries";
  public static final String BOOKS = "/books";
  public static final String PATH_LIBRARY_ID = "/{libraryId}";
  public static final String PATH_BOOK_ID = "/{bookId}";
  public static final String PATH_H2_CONSOLE = "/h2-console/**";

}
