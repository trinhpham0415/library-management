package com.example.library.constant;

public class Message {

  public static final String LIBRARY_NOT_FOUND = "library.error.notFound";
  public static final String BOOK_NOT_FOUND = "book.error.notFound";

}
