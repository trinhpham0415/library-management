package com.example.library.constant;

public enum Role {

  ADMIN("ADMIN"),
  USER("USER");

  private String value;

  public String getValue() {
    return this.value;
  }

  Role(String value) {
    this.value = value;
  }

}
