package com.example.library.constant;

public class Constants {

  public static final String EMPTY_STRING = "";
  public static final String SEMICOLON = "; ";
  public static final String COLON = ":";
  public static final String AUTHORIZATION = "Authorization";
  public static final String BEARER = "Bearer ";
  public static final int TOKEN_INDEX = 7;
  public static final String LIBRARY_ID = "libraryId";
  public static final String BOOK_ID = "bookId";
  public static final String USER = "user";
  public static final String ROLE_USER = "USER";
  public static final String ADMIN = "admin";
  public static final String ROLE_ADMIN = "ADMIN";

}
