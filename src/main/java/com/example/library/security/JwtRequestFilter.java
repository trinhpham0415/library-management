package com.example.library.security;

import com.example.library.constant.Constants;
import com.example.library.service.JwtTokenService;
import io.jsonwebtoken.Claims;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

  private final JwtTokenService jwtTokenService;
  private final UserDetailsService userDetailsService;

  public JwtRequestFilter(JwtTokenService jwtTokenService,
                          UserDetailsService userDetailsService) {
    this.jwtTokenService = jwtTokenService;
    this.userDetailsService = userDetailsService;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                  FilterChain filterChain) throws ServletException, IOException {

    final String authorizationHeader = request.getHeader(Constants.AUTHORIZATION);

    if (authorizationHeader != null && authorizationHeader.startsWith(Constants.BEARER)) {
      String token = authorizationHeader.substring(Constants.TOKEN_INDEX);
      String userNameFromToken;
      Claims claims = jwtTokenService.getClaimsFromToken(token);

      if (jwtTokenService.isValidToken(claims) && (userNameFromToken = jwtTokenService.getUsername(claims)) != null) {
        UserDetails user = userDetailsService.loadUserByUsername(userNameFromToken);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
            = new UsernamePasswordAuthenticationToken(user.getUsername(), null, user.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      }
    }
    filterChain.doFilter(request, response);
  }

}