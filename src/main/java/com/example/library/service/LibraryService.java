package com.example.library.service;

import com.example.library.domain.request.NewLibrary;
import com.example.library.domain.response.Response;

public interface LibraryService {

  Response addLibrary(NewLibrary newLibrary);
  Response getLibraries();
  Response deleteLibrary(Long libraryId);

}
