package com.example.library.service;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;

public interface JwtTokenService {

  String generateToken(UserDetails userDetails);
  String getUsername(Claims claims);
  boolean isValidToken(Claims claims);
  Claims getClaimsFromToken(String token);

}
