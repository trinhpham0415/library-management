package com.example.library.service;

import com.example.library.domain.request.NewBook;
import com.example.library.domain.response.Response;

public interface BookService {

  Response addNewBookToLibrary(Long libraryId, NewBook newBook);
  Response getBooks();
  Response linkBookToLibrary(Long libraryId, Long bookId);
  Response removeBookFromLibrary(Long bookId);

}
