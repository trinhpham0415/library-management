package com.example.library.service.impl;

import com.example.library.constant.Message;
import com.example.library.domain.request.NewLibrary;
import com.example.library.domain.response.Response;
import com.example.library.entity.Library;
import com.example.library.exception.ResourceNotFoundException;
import com.example.library.repository.LibraryRepository;
import com.example.library.service.LibraryService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LibraryServiceImpl extends BaseService implements LibraryService {

  private final LibraryRepository libraryRepository;

  public LibraryServiceImpl(LibraryRepository libraryRepository) {
    this.libraryRepository = libraryRepository;
  }

  @Override
  public Response addLibrary(NewLibrary newLibrary) {
    Library library = new Library(newLibrary.getName(), newLibrary.getBooks());
    library.getBooks().forEach(book -> book.setLibrary(library));
    return new Response(HttpStatus.CREATED.getReasonPhrase(), libraryRepository.save(library));
  }

  @Override
  public Response getLibraries() {
    return new Response(HttpStatus.OK.getReasonPhrase(), libraryRepository.findAll());
  }

  @Override
  @Transactional
  public Response deleteLibrary(Long libraryId) {
    return libraryRepository.findById(libraryId)
      .map(library ->  {
        libraryRepository.delete(library);
        return new Response(HttpStatus.OK.getReasonPhrase());
      })
      .orElseThrow(() -> new ResourceNotFoundException(getMessage(Message.LIBRARY_NOT_FOUND, libraryId)));
  }

}
