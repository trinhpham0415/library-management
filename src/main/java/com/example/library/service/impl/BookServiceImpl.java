package com.example.library.service.impl;

import com.example.library.constant.Message;
import com.example.library.domain.request.NewBook;
import com.example.library.domain.response.Response;
import com.example.library.entity.Book;
import com.example.library.entity.Library;
import com.example.library.exception.ResourceNotFoundException;
import com.example.library.repository.BookRepository;
import com.example.library.repository.LibraryRepository;
import com.example.library.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl extends BaseService implements BookService {

  private final BookRepository bookRepository;
  private final LibraryRepository libraryRepository;

  public BookServiceImpl(BookRepository bookRepository, LibraryRepository libraryRepository) {
    this.bookRepository = bookRepository;
    this.libraryRepository = libraryRepository;
  }

  @Override
  public Response addNewBookToLibrary(Long libraryId, NewBook newBook) {
    return libraryRepository.findById(libraryId)
      .map(library -> new Response(HttpStatus.OK.getReasonPhrase(),
          bookRepository.save(new Book(newBook.getTitle(), library))))
      .orElseThrow(() -> new ResourceNotFoundException(getMessage(Message.LIBRARY_NOT_FOUND, libraryId)));
  }

  @Override
  public Response getBooks() {
    return new Response(HttpStatus.OK.getReasonPhrase(), bookRepository.findAll());
  }

  @Override
  public Response linkBookToLibrary(Long libraryId, Long bookId) {
    Library library = libraryRepository.findById(libraryId)
        .orElseThrow(() -> new ResourceNotFoundException(getMessage(Message.LIBRARY_NOT_FOUND, libraryId)));

    return bookRepository.findById(bookId)
        .map(book -> {
          book.setLibrary(library);
          return new Response(HttpStatus.OK.getReasonPhrase(), bookRepository.save(book));
        })
        .orElseThrow(() -> new ResourceNotFoundException(getMessage(Message.BOOK_NOT_FOUND, bookId)));
  }

  @Override
  public Response removeBookFromLibrary(Long bookId) {
    return bookRepository.findById(bookId)
        .map(book -> {
          book.setLibrary(null);
          return new Response(HttpStatus.OK.getReasonPhrase(), bookRepository.save(book));
        })
        .orElseThrow(() -> new ResourceNotFoundException(getMessage(Message.BOOK_NOT_FOUND, bookId)));
  }

}
