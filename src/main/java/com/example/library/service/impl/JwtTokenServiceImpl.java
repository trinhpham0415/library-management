package com.example.library.service.impl;

import com.example.library.service.JwtTokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {

  @Value("${jwt.secret}")
  private String jwtSecret;

  private static final long JWT_VALIDITY = 15 * 60 * 1000; //15 minutes

  @Override
  public String generateToken(UserDetails userDetails) {
    return doGenerateToken(new HashMap<>(), userDetails.getUsername());
  }

  private String doGenerateToken(HashMap<String, Object> claims, String subject) {
    String token = Jwts.builder().setClaims(claims)
        .setSubject(subject)
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + JWT_VALIDITY))
        .signWith(SignatureAlgorithm.HS256, jwtSecret)
        .compact();
    return token;
  }

  public String getUsername(Claims claims) {
    return getClaim(claims, Claims::getSubject);
  }

  private <T> T getClaim(Claims claims, Function<Claims, T> function) {
    return function.apply(claims);
  }

  public Claims getClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
  }

  public boolean isValidToken(Claims claims) {
    Date expirationDate = getClaim(claims, Claims::getExpiration);
    return expirationDate != null && expirationDate.after(new Date());
  }

}
