package com.example.library.service.impl;

import com.example.library.constant.Constants;
import java.util.Locale;
import javax.annotation.Resource;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class BaseService {

  @Resource
  private MessageSource messageSource;

  public final String getMessage(String code, Object... args) {
    return messageSource.getMessage(code, args, Constants.EMPTY_STRING, Locale.US);
  }

}
